import { LoginComponent } from './../components/login/login.component';
import { RegisterComponent } from './../components/register/register.component';
import { StudentsComponent } from '../components/students/students.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StudentInformationComponent } from '../components/student-information/student-information.component';
import { AuthGuard } from '../helpers/auth-guard';

const routes: Routes = [
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: 'students', component: StudentsComponent , canActivate: [AuthGuard],
                  data: {
                    role: ['Admin']
                  }
                },
  { path: 'studentInformation', component: StudentInformationComponent, canActivate: [AuthGuard],
  data: {
    role: ['Student']
  }
},
  { path: '', redirectTo: '/students', pathMatch: 'full' }
];

@NgModule({

  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
