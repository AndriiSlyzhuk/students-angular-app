export interface User {
  userId: number;
  login: string;
  role: string;
  token: string;
}
