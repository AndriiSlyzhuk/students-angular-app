﻿using Microsoft.EntityFrameworkCore;
using Students_Angular_App.DAL.Models;
using Students_Angular_App.DLL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Students_Angular_App.DAL.Context
{
    public class StudentsAppContext : DbContext
    {
        public StudentsAppContext(DbContextOptions<StudentsAppContext> options)
           : base(options)
        {
            Database.EnsureCreated();   // создаем базу данных при первом обращении
        }

        public DbSet<Student> Students { get; set; }

        public DbSet<StudentInformation> StudentInformations { get; set; }

        public DbSet<User> Users { get; set; }
    }
}