﻿using Students_Angular_App.DAL.Models;
using Students_Angular_App.DLL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Students_Angular_App.DAL.Models
{
    public class Student
    {
        [Key]
        public long StudentId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Surname { get; set; }

        public DateTime BirthDate { get; set; }

        [ForeignKey("UserId")]
        public long? UserId { get; set; }
        User User { get; set; }

        public StudentInformation StudentInformation { get; set; }
    }
}
